import {LessonPin} from "./lesson-pin";
import {ModelChanges} from "../../shared/model-update/model-changes";
import {Model} from "./model";
import {LessonUserData} from "../data/lesson-user-data";
import {PinData} from "../../shared/data/pin-data";
import {ModelChange} from "../../shared/model-update/model-change";

declare const _: any;

export class LessonUser extends Model implements LessonUserData {

  public userId: string;
  public pins: LessonPin[] = [];
  protected _pinAddAction: string;
  protected _pinRemoveAction: string;

  modelUpdate(data: LessonUserData, changes: ModelChanges) {
    this.userId = data.userId;
    this.updatePins(data.pins, changes);
  }

  public addPin(data: PinData, changes: ModelChanges) {
    const pin: LessonPin = new LessonPin(data);
    this.pins.push(pin);
    changes.push(new ModelChange(this._pinAddAction, pin));
  }

  public removePin(data: PinData, changes: ModelChanges) {
    const pin: LessonPin = _.find(this.pins, p => p.id == data.id);

    if (!pin) {
      console.log('LessonUser@removePin pin not found, id:', data.id);
      return;
    }

    this.pins.splice(_.indexOf(this.pins, pin), 1);
    changes.push(new ModelChange(this._pinRemoveAction, pin));
  }

  public removePins(changes: ModelChanges) {
    while (this.pins.length)
      this.removePin(this.pins[0], changes);
  }

  public hasPin(data: PinData): boolean {
    return _.find(this.pins, p => p.id == data.id);
  }

  private updatePins(data: PinData[], changes: ModelChanges) {
    const pins: PinData[] = _.xorBy(this.pins, data, p => p.id);

    _.forEach(pins, pin => {
      const index: number = _.findIndex(this.pins, pin);

      if (index != -1)
        this.removePin(pin, changes);

      else
        this.addPin(pin, changes);
    });
  }

}
