import {ModelChange} from "./model-change";

export class ModelChanges extends Array<ModelChange> {}
