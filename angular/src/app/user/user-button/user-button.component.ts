import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-user-button',
  templateUrl: './user-button.component.html',
  styleUrls: ['./user-button.component.scss']
})
export class UserButtonComponent implements OnInit {

  @Input() label: string;

  constructor() { }

  ngOnInit() {
  }

}
