import {Injectable} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {UtilService} from "../shared/util.service";
import {Card} from "./card";
import {Observable} from "rxjs";
import * as _ from "lodash";

@Injectable()
export class CardService {

  constructor(
    private _apiService: ApiService,
    private _utilService: UtilService
  ) {}

  public getCard(id: string): Observable<Card> {
    return this._apiService.get('cards/' + id)
    .do(card => this.fixUrls(card.data));
  }

  private fixUrls(data: any) {
    _.forEach(data, (value, key) => {
      if (key == 'src') {
        if (!_.startsWith('http'))
          data.src = this._utilService.fixServerUrl(data.src);
      }

      else if (_.isArray(value))
        _.forEach(value, item => this.fixUrls(item));

      else if (_.isObject(value))
        this.fixUrls(value);
    });
  }

}
