import {Component, OnDestroy, OnInit} from '@angular/core';
import {OverseerService} from "./core/overseer.service";
import {UserService} from "./user/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public isRegistered: boolean;
  public studentListEnabled: boolean = false;
  public contextBarEnabled: boolean = false;

  constructor(
    overseerService: OverseerService,
    userService: UserService
  ) {
    userService.isRegistered$.subscribe(isRegistered => this.isRegistered = isRegistered);
  }

  ngOnInit() {
    const w: any = window;
    if (w.plugins)
      w.plugins.insomnia.keepAwake();
  }

  ngOnDestroy() {
    const w: any = window;
    if (w.plugins)
      w.plugins.insomnia.allowSleepAgain();
  }

}
