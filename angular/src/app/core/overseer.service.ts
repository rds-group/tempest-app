import {Injectable} from '@angular/core';
import {SocketService} from "../socket/socket.service";
import {UserService} from "../user/user.service";
import {Router} from "@angular/router";
import {ApplicationService} from "../shared/application.service";

@Injectable()
export class OverseerService {

  constructor(
    router: Router,
    applicationService: ApplicationService,
    userService: UserService,
    socketService: SocketService
  ) {
    userService.user$.subscribe(user => {
      if (user)
        router.navigate(['/connect']);

      else
        router.navigate(['/user/login']);
    });

    socketService.connection$.subscribe(connection => {
      if (!connection.isOpen)
        router.navigate(['/connect']);
    });

    document.addEventListener('pause', () => applicationService.setActive(false));
    document.addEventListener('resume', () => applicationService.setActive(true));
  }

}
