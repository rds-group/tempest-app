import {Routes} from "@angular/router";
import {CardSetListComponent} from "./card-set-list/card-set-list.component";
import {CardSetDetailComponent} from "./card-set-detail/card-set-detail.component";
import {UserGuard} from "../user/user-guard.service";

export const routes: Routes = [
  {
    path: 'card-set',
    canActivate: [UserGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: CardSetListComponent
      },
      {
        path: ':id',
        component: CardSetDetailComponent,
        data: {navigationBack: ['card-set', 'list']}
      }
    ]
  }
];
