import {Component, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output} from '@angular/core';
import {LessonService} from "../../lesson/lesson.service";

@Component({
  selector: 'app-context-bar',
  templateUrl: './context-bar.component.html',
  styleUrls: ['./context-bar.component.scss']
})
export class ContextBarComponent implements OnInit, OnChanges {

  @Input() enabled: boolean;
  @HostBinding('class.enabled') isEnabled: boolean;
  @Output() enabledChange: EventEmitter<boolean> = new EventEmitter();

  public isLocked: boolean;

  constructor(private _lessonService: LessonService) {}

  ngOnInit() {
    this._lessonService.lesson$.subscribe(lesson => this.isLocked = lesson && lesson.isLocked);
  }

  ngOnChanges() {
    this.isEnabled = this.enabled;
  }

  public onClearLessonClick() {
    this._lessonService.clear();
    this.close();
  }

  public onLockLesson() {
    this._lessonService.lock(!this.isLocked);
    this.close();
  }

  public onEndLessonClick() {
    this._lessonService.end();
    this.close();
  }

  public close() {
    this.isEnabled = this.enabled = false;
    this.enabledChange.emit(this.enabled);
  }

}
