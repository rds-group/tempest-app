import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ConfigService} from "../../shared/config.service";
import {UserService} from "../user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public data: {username: string, password: string} = {
    username: '',
    password: ''
  };

  public isSending: boolean = false;
  public error: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _configService: ConfigService,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      if (params.error)
        this.error = params.error;
    });

    this.data.username = this._configService.user.username;
    this.data.password = this._configService.user.password;
  }

  public onSubmit() {
    if (!this.data.username || !this.data.password)
      return;

    this.isSending = true;
    this.error = null;

    this._userService.login(this.data.username, this.data.password)
    .subscribe({error: () => {
      this.isSending = false;
      this.error = 'Incorrect username or password';
    }});
  }

}
