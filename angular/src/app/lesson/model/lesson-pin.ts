import {Model} from "./model";
import {PinData} from "../../shared/data/pin-data";

export class LessonPin extends Model implements PinData {

  public id: string;
  public x: number;
  public y: number;

}
