import {Injectable} from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {UserService} from "./user.service";

@Injectable()
export class UserGuard implements CanActivate {

  constructor(
    private _router: Router,
    private _userService: UserService
  ) {}

  canActivate() {
    const loggedIn: boolean = this._userService.user$.getValue() != null;

    if (!loggedIn)
      this._router.navigate(['/user/login']);

    return loggedIn;
  }

}
