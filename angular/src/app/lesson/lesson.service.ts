import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {SocketService} from "../socket/socket.service";
import {Lesson} from "./model/lesson";
import {ModelChanges} from "../shared/model-update/model-changes";
import {AppAction} from "../shared/app-action";
import {LessonData} from "./data/lesson-data";
import {LessonUser} from "./model/lesson-user";
import {UserService} from "../user/user.service";
import {User} from "../user/user";
import {PinData} from "../shared/data/pin-data";
import {ModelChange} from "../shared/model-update/model-change";
import {SocketConnection} from "../socket/socket-connection";
import {CardSet} from "../card-set/card-set";
import {Card} from "../card/card";
import * as _ from "lodash";

declare const cuid: any;

@Injectable()
export class LessonService {

  public lesson$: BehaviorSubject<Lesson> = new BehaviorSubject(null);
  public changes$: BehaviorSubject<ModelChanges> = new BehaviorSubject(null);
  private _cardSet: CardSet;

  constructor(
    private _socketService: SocketService,
    private _userService: UserService
  ) {
    this.bindSocket();
  }

  public dispose() {
    this.update(null, null);
  }

  public start(cardSet: CardSet, cardId: string): Observable<LessonData> {
    this._cardSet = cardSet;

    return this._socketService.emit(AppAction.lessonStart, {cardId: cardId})
    .do(data => console.log('lesson start'));
  }

  public end() {
    this.update(null, null);

    this._socketService.emit(AppAction.lessonEnd)
    .subscribe();
  }

  public clear() {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@clear lesson not found');
      return null;
    }

    const changes: ModelChanges = new ModelChanges();
    lesson.removePins(changes);

    this.update(lesson, changes);

    this._socketService.emit(AppAction.lessonClear)
    .subscribe();
  }

  public nextCard(): Observable<LessonData> {
    const card: Card = this.getNextCard();
    if (!card)
      return Observable.throw(null);

    return this.start(this._cardSet, card._id);
  }

  public isNextCard(): boolean {
    return !!this.getNextCard();
  }

  public previousCard(): Observable<LessonData> {
    const card: Card = this.getPreviousCard();
    if (!card)
      return Observable.throw(null);

    return this.start(this._cardSet, card._id);
  }

  public isPreviousCard(): boolean {
    return !!this.getPreviousCard();
  }

  public clearUser(lessonUser: LessonUser) {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@clearUser lesson not found');
      return;
    }

    const user: User = this._userService.getRegisteredUser(lessonUser.userId);
    if (!user) {
      console.error('LessonService@clearUser user not found');
      return;
    }

    const changes: ModelChanges = new ModelChanges();
    lessonUser.removePins(changes);

    this.update(lesson, changes);

    const action: string = user.type == User.Type.Teacher ? AppAction.lessonTeacherClear : AppAction.lessonStudentClear;

    this._socketService.emit(action, {userId: user._id})
    .subscribe();
  }

  public lock(lock: boolean) {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@lock lesson not found');
      return;
    }

    lesson.isLocked = lock;

    const action: string = lock ? AppAction.lessonLock : AppAction.lessonUnlock;

    this._socketService.emit(action)
    .subscribe();
  }

  public addPin(data: {x: number, y: number}) {
    const user: LessonUser = this.getUser();
    if (!user) {
      console.error('LessonService@addPin user not found');
      return;
    }

    const lesson: Lesson = this.lesson$.getValue();

    const pin: PinData = _.assign({id: cuid()}, data);
    const changes: ModelChanges = new ModelChanges();
    user.addPin(pin, changes);

    this.update(lesson, changes);

    const change: ModelChange = changes[0];
    this._socketService.emit(change.action, pin)
    .subscribe();
  }

  public removePin(pin: PinData) {
    const user: LessonUser = this.getUserByPin(pin);
    if (!user) {
      console.error('LessonService@removePin user not found');
      return;
    }

    const lesson: Lesson = this.lesson$.getValue();

    const changes: ModelChanges = new ModelChanges();
    user.removePin(pin, changes);

    this.update(lesson, changes);

    const change: ModelChange = changes[0];
    this._socketService.emit(change.action, pin)
    .subscribe();
  }

  public getUser(id?: string): LessonUser {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@getUser lesson not found');
      return null;
    }

    if (id) {
      if (lesson.teacher.userId == id)
        return lesson.teacher;

      return _.find(lesson.students, student => student.userId == id);
    }

    const user: User = this._userService.user$.getValue();

    if (lesson.teacher.userId == user._id)
      return lesson.teacher;

    return _.find(lesson.students, s => s.userId == user._id);
  }

  public getUserByPin(pin: PinData): LessonUser {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@getUserByPin lesson not found');
      return null;
    }

    if (lesson.teacher.hasPin(pin))
      return lesson.teacher;

    return _.find(lesson.students, s => s.hasPin(pin));
  }

  private bindSocket() {
    const connection: SocketConnection = this._socketService.connection$.getValue();

    connection.on(AppAction.lessonUpdate)
    .subscribe(data => this.processLessonData(data));

    connection.on(AppAction.lessonEnd)
    .subscribe(() => {
      console.log('lesson end');

      this.update(null, null);
    });

    this._socketService.connection$.subscribe(connection => {
      if (!connection.isOpen)
        this.update(null, null);
    });
  }

  private processLessonData(data: LessonData) {
    if (!data)
      this.update(null, null);

    let lesson: Lesson = this.lesson$.getValue();
    if (!lesson)
      lesson = new Lesson();

    const changes: ModelChanges = new ModelChanges();
    lesson.modelUpdate(data, changes);

    this.update(lesson, changes);
  }

  private update(lesson: Lesson, changes: ModelChanges) {
    console.log('lesson update', lesson, changes);

    this.lesson$.next(lesson);
    this.changes$.next(changes);
  }

  private getNextCard(): Card {
    const card: Card = this.getCurrentCard();
    if (!card)
      return null;

    const index: number = this.getCardIndex(card);
    if (index == this._cardSet.cards.length - 1)
      return null;

    return <Card>this._cardSet.cards[index + 1];
  }

  private getPreviousCard(): Card {
    const card: Card = this.getCurrentCard();
    if (!card)
      return null;

    const index: number = this.getCardIndex(card);
    if (index == 0)
      return null;

    return <Card>this._cardSet.cards[index - 1];
  }

  private getCurrentCard(): Card {
    const lesson: Lesson = this.lesson$.getValue();
    if (!lesson) {
      console.error('LessonService@getCurrentCard lesson not found');
      return null;
    }

    return this._cardSet ? _.find(<Card[]>this._cardSet.cards, card => card._id == lesson.card.cardId) : null;
  }

  private getCardIndex(card: Card): number {
    return this._cardSet ? _.indexOf(<Card[]>this._cardSet.cards, card) : null;
  }

}
