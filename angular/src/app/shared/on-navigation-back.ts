export interface OnNavigationBack {

  isEnabled(): boolean;
  onNavigationBack();

}
