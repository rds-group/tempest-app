import {LessonCard} from "./lesson-card";
import {LessonTeacher} from "./lesson-teacher";
import {LessonStudent} from "./lesson-student";
import {ModelChanges} from "../../shared/model-update/model-changes";
import {LessonData} from "../data/lesson-data";
import {Model} from "./model";
import {LessonStudentData} from "../data/lesson-student-data";
import {AppAction} from "../../shared/app-action";
import {ModelChange} from "../../shared/model-update/model-change";

declare const _: any;

export class Lesson extends Model implements LessonData {

  public isLocked: boolean;
  public card: LessonCard = new LessonCard();
  public teacher: LessonTeacher = new LessonTeacher();
  public students: LessonStudent[] = [];

  modelUpdate(data: LessonData, changes: ModelChanges) {
    this.isLocked = data.isLocked;
    this.card.modelUpdate(data.card, changes);
    this.teacher.modelUpdate(data.teacher, changes);
    this.updateStudents(data.students, changes);
  }

  public removePins(changes? :ModelChanges) {
    this.teacher.removePins(changes);

    _.forEach(this.students, student => student.removePins(changes));
  }

  private updateStudents(data: LessonStudentData[], changes: ModelChanges) {
    const students: LessonStudentData[] = _.unionBy(this.students, data, s => s.userId);

    _.forEach(students, student => {
      const indexInCurrent: number = _.findIndex(this.students, s => s.userId == student.userId);
      const indexInNew: number = _.findIndex(data, s => s.userId == student.userId);

      if (indexInCurrent != -1 && indexInNew != -1) {
        const studentData: LessonStudentData = _.find(data, s => s.userId == student.userId);
        student.modelUpdate(studentData, changes);

      } else if (indexInCurrent != -1) {
        student.removePins(changes);
        this.students.splice(indexInCurrent, 1);
        changes.push(new ModelChange(AppAction.lessonStudentRemove, student));

      } else {
        const newStudent: LessonStudent = new LessonStudent();
        this.students.push(newStudent);
        changes.push(new ModelChange(AppAction.lessonStudentAdd, newStudent));
        newStudent.modelUpdate(student, changes);
      }
    });
  }

}
