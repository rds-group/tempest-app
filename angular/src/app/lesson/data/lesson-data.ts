import {LessonTeacherData} from "./lesson-teacher-data";
import {LessonStudentData} from "./lesson-student-data";
import {LessonCardData} from "./lesson-card-data";

export interface LessonData {

  isLocked: boolean;
  card: LessonCardData;
  teacher: LessonTeacherData;
  students: LessonStudentData[];

}
