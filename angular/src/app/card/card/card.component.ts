import {AfterViewInit, Component, EventEmitter, Input, ViewChild} from '@angular/core';
import {Card} from "../card";
import {FabricComponent} from "../fabric/fabric.component";
import {CardPin} from "../card-pin";
import {PinData} from "../../shared/data/pin-data";
import {CardPinType} from "../card-pin-type.enum";

declare const _: any;

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements AfterViewInit {

  @Input() card: Card;

  @ViewChild(FabricComponent) fabric: FabricComponent;

  public onPinAdd: EventEmitter<{x: number, y: number}> = new EventEmitter();
  public onPinRemove: EventEmitter<PinData> = new EventEmitter();
  private _pins: CardPin[] = [];

  constructor() {}

  ngAfterViewInit() {
    this.fabric.onClick.subscribe(data => this.onFabricClick(data));
    this.fabric.onObjectClick.subscribe(object => this.onFabricObjectClick(object));
  }

  public addPin(data: {data: PinData, type: CardPinType, color: string, label: string}) {
    const pin: CardPin = new CardPin(data);
    pin.fabricObject = this.fabric.addPin(data);
    this._pins.push(pin);
  }

  public removePin(data: PinData) {
    const pinIndex: number = _.findIndex(this._pins, p => p.data.id == data.id);
    if (pinIndex == -1) {
      console.error('CardComponent@removePin pin not found');
      return;
    }

    this.fabric.removePin(this._pins[pinIndex].fabricObject);
    this._pins.splice(pinIndex, 1);
  }

  private onFabricClick(data: {x: number, y: number}) {
    this.onPinAdd.emit(data);
  }

  private onFabricObjectClick(fabricObject: any) {
    const pin: CardPin = _.find(this._pins, p => p.fabricObject == fabricObject);
    if (!pin) {
      console.error('CardComponent@onFabricObjectClick pin not found');
      return;
    }

    this.onPinRemove.emit(pin.data);
  }

}
