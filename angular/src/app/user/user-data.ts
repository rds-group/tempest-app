import {User} from "./user";

export interface UserData {

  _id: string;
  type: User.Type | string;
  username: string;
  password: string;
  displayName: string;
  symbol: string;
  color: string;
  isActive: boolean;

}
