import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSetListItemComponent } from './card-set-list-item.component';

describe('CardSetListItemComponent', () => {
  let component: CardSetListItemComponent;
  let fixture: ComponentFixture<CardSetListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSetListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSetListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
