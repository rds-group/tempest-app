import {Card} from "../card/card";

export class CardSet {

  public _id: string;
  public name: string;
  public cards: string[] | Card[];
  public tags: string[];
  public preview: string;

}
