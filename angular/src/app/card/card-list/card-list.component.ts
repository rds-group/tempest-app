import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card} from "../card";

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {

  @Input() cards: Card[];
  @Output() onCardClick: EventEmitter<Card> = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

}
