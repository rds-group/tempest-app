import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";

import {UserService} from "./user.service";

import {LoginComponent} from './login/login.component';
import {UserButtonComponent} from './user-button/user-button.component';

import {routes} from "./routes";
import {UserGuard} from "./user-guard.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [LoginComponent, UserButtonComponent],
  providers: [
    UserService,
    UserGuard
  ]
})
export class UserModule {
}
