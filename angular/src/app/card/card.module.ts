import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";

import {CardService} from "./card.service";

import {CardListComponent} from './card-list/card-list.component';
import {CardListItemComponent} from './card-list-item/card-list-item.component';
import {CardComponent} from './card/card.component';

import {routes} from "./routes";
import { FabricComponent } from './fabric/fabric.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [CardListComponent, CardListItemComponent, CardComponent, FabricComponent],
  providers: [
    CardService
  ],
  exports: [
    CardListComponent,
    CardComponent
  ]
})
export class CardModule {
}
