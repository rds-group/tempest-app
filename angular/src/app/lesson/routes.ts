import {Routes} from "@angular/router";
import {LessonComponent} from "./lesson/lesson.component";
import {UserGuard} from "../user/user-guard.service";
import {InjectionToken} from "@angular/core";
import {NavigationBackService} from "./navigation-back.service";

export const navigationBack: InjectionToken<NavigationBackService> = new InjectionToken('app.lesson.navigation-back');

export const routes: Routes = [
  {
    path: 'lesson',
    component: LessonComponent,
    canActivate: [UserGuard],
    data: {navigationBack: navigationBack}
  }
];
