import { TestBed, inject } from '@angular/core/testing';

import { OverseerService } from './overseer.service';

describe('OverseerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OverseerService]
    });
  });

  it('should ...', inject([OverseerService], (service: OverseerService) => {
    expect(service).toBeTruthy();
  }));
});
