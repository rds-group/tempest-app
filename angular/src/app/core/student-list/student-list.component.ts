import {Component, HostBinding, Input, OnChanges, OnInit} from '@angular/core';
import {UserService} from '../../user/user.service';
import {RegisteredUsers} from "../../user/registered-users";
import {UserPipe} from "../user.pipe";
import {User} from "../../user/user";
import {LessonService} from "../../lesson/lesson.service";
import {LessonUser} from "../../lesson/model/lesson-user";

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss'],
  providers: [UserPipe]
})
export class StudentListComponent implements OnInit, OnChanges {

  @Input() enabled: boolean;
  @HostBinding('class.enabled') isEnabled: boolean;

  public isLesson: boolean;
  public user: User;
  public userType = User.Type;
  public registeredUsers: RegisteredUsers;

  constructor(
    private _userService: UserService,
    private _lessonService: LessonService
  ) {}

  ngOnInit() {
    this._userService.user$.subscribe((user) => {
      this.user = user;
    });

    this._userService.registeredUsers$.subscribe(registeredUsers => {
      this.registeredUsers = registeredUsers;
    });

    this._lessonService.lesson$.subscribe(lesson => this.isLesson = !!lesson);
  }

  ngOnChanges() {
    this.isEnabled = this.enabled;
  }

  onStudentClearClick(user) {
    const lessonUser: LessonUser = this._lessonService.getUser(user._id);
    this._lessonService.clearUser(lessonUser);
  }

}
