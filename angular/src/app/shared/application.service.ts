import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class ApplicationService {

  isActive$: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor() {
    document.addEventListener('backbutton', e => {
      e.preventDefault();
      e.stopImmediatePropagation();
      return false;
    });
  }

  public setActive(isActive: boolean) {
    if (isActive != this.isActive$.value)
      this.isActive$.next(isActive);
  }

}
