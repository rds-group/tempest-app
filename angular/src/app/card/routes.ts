import {Routes} from "@angular/router";
import {CardListComponent} from "./card-list/card-list.component";
import {UserGuard} from "../user/user-guard.service";

export const routes: Routes = [
  {
    path: 'card',
    canActivate: [UserGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: CardListComponent
      }
    ]
  }
];
