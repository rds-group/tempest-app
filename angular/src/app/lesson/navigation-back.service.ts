import {Injectable} from '@angular/core';
import {OnNavigationBack} from "../shared/on-navigation-back";
import {UserService} from "../user/user.service";
import {LessonService} from "./lesson.service";
import {User} from "../user/user";

@Injectable()
export class NavigationBackService implements OnNavigationBack {

  constructor(
    private _userService: UserService,
    private _lessonService: LessonService
  ) { }

  isEnabled(): boolean {
    const user: User = this._userService.user$.value;
    return user && user.type == User.Type.Teacher;
  }

  onNavigationBack() {
    this._lessonService.end();
  }

}
