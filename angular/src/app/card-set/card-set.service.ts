import {Injectable} from '@angular/core';
import {ApiService} from "../shared/api.service";
import {UtilService} from "../shared/util.service";
import {CardSet} from "./card-set";
import {Card} from "../card/card";
import {Observable} from "rxjs/Observable";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import * as _ from "lodash";

@Injectable()
export class CardSetService {

  public cardSets$: BehaviorSubject<CardSet[]> = new BehaviorSubject([]);

  constructor(
    private _apiService: ApiService,
    private _utilService: UtilService
  ) { }

  public loadCardSets(): Observable<CardSet[]> {
    return this._apiService.get('card-sets')
    .do(cardSets => this.cardSets$.next(cardSets));
  }

  public getCardSet(id: string): Observable<CardSet> {
    return this._apiService.get('card-sets/' + id)
    .do(cardSet => {
      _.forEach(cardSet.cards, (card: Card) => {
        card.preview = card.preview
          ? this._utilService.fixServerUrl('card/img/' + card._id + '/' + _.trimStart(card.preview, '/'))
          : null;
      });
    });
  }

}
