import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {SocketConnection} from "./socket-connection";
import {ConfigService} from "../shared/config.service";

declare const io: any;

@Injectable()
export class SocketService {

  public connection$: BehaviorSubject<SocketConnection> = new BehaviorSubject(new SocketConnection());

  constructor(private _config: ConfigService) {
    this.start();
  }

  public emit(eventName: string, data?: any): Observable<any> {
    return new Observable(observer => {
      const connection: SocketConnection = this.connection$.getValue();

      if (!connection.isOpen) {
        console.error('SocketService@emit connection not open');

        observer.error('connection not open');
        return;
      }

      if (eventName != 'latency.ping' && eventName != 'latency.report') {
        console.log('socket emit', eventName, data);
      }

      connection.socket.emit(eventName, data, response => {
        if (response) {
          observer.next(response);
          observer.complete();
        }
        else
          observer.error(response);
      });
    });
  }

  private start() {
    const socket: any = io(this._config.socket.url);

    const connection: SocketConnection = this.connection$.getValue();
    connection.socket = socket;

    socket.on('connect', () => {
      console.log('socket connect success');

      connection.isOpen = true;
      this.connection$.next(connection);
    });

    socket.on('connect_error', () => {
      console.log('socket connect error');
    });

    socket.on('connect_timeout', () => {
      console.log('socket connect error');
    });

    socket.on('error', error => {
      console.log('socket error:', error);
    });

    socket.on('disconnect', () => {
      console.log('socket disconnect');

      connection.isOpen = false;
      this.connection$.next(connection);
    });
  }

}
