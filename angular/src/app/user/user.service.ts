import {Injectable} from '@angular/core';
import {User} from "./user";
import {BehaviorSubject, Observable} from "rxjs";
import {ApplicationService} from "../shared/application.service";
import {ApiService} from "../shared/api.service";
import {SocketService} from "../socket/socket.service";
import {AppAction} from "../shared/app-action";
import {RegisteredUsers} from "./registered-users";
import {SocketConnection} from "../socket/socket-connection";
import * as _ from "lodash";

@Injectable()
export class UserService {

  public user$: BehaviorSubject<User> = new BehaviorSubject(null);
  public isRegistered$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public registeredUsers$: BehaviorSubject<RegisteredUsers> = new BehaviorSubject(new RegisteredUsers());

  constructor(
    private _applicationService: ApplicationService,
    private _apiService: ApiService,
    private _socketService: SocketService
  ) {
    this.bindEvents();
    this.bindSocket();
    this.bindActive();
  }

  public login(username: string, password: string): Observable<User> {
    return this._apiService.post('user/login', {username: username, password: password})
    .map(data => new User(_.assign(data, {password: password})))
    .do(user => {
      this._apiService.username = user.username;
      this._apiService.password = user.password;

      this.user$.next(user);
    });
  }

  public logout() {
    if (this.user$.getValue()) {
      this._apiService.username = null;
      this._apiService.password = null;

      this._socketService.emit(AppAction.userUnregister)
      .subscribe();

      this.user$.next(null);
      this.isRegistered$.next(false);

      const registeredUsers: RegisteredUsers = this.registeredUsers$.getValue();
      registeredUsers.clear();
      this.registeredUsers$.next(registeredUsers);
    }
  }

  public register(): Observable<any> {
    const user: User = this.user$.getValue();

    return this._socketService.emit(AppAction.userRegister, {username: user.username, password: user.password})
    .do(user => {
      console.log('user register success');

      this.isRegistered$.next(true);

      this.sendActive();
    })
    .catch(error => {
      console.log('user register error');

      return Observable.throw('This user is taken, try a different one');
    });
  }

  public getRegisteredUser(id?: string): User {
    if (!id)
      id = this.user$.getValue()._id;

    return this.registeredUsers$.getValue().getUser(id);
  }

  private bindEvents() {
    this.user$.subscribe(user => console.log('user', user));
    this.registeredUsers$.subscribe(users => console.log('registered users', users));
  }

  private bindSocket() {
    const connection: SocketConnection = this._socketService.connection$.getValue();

    connection.on(AppAction.registeredUsersUpdate)
    .subscribe(users => {
      const registeredUsers: RegisteredUsers = this.registeredUsers$.getValue();
      registeredUsers.modelUpdate({users: users});
      this.registeredUsers$.next(registeredUsers);
    });

    this._socketService.connection$.subscribe(connection => {
      if (!connection.isOpen) {
        this.isRegistered$.next(false);

        const registeredUsers: RegisteredUsers = this.registeredUsers$.getValue();
        registeredUsers.clear();
        this.registeredUsers$.next(registeredUsers);
      }
    });
  }

  private bindActive() {
    this._applicationService.isActive$.filter(() => this.isRegistered$.value)
    .subscribe(() => this.sendActive());
  }

  private sendActive() {
    this._socketService.emit(AppAction.userStatus, this._applicationService.isActive$.value)
    .subscribe();
  }

}
