import {Routes} from "@angular/router";
import {LoginComponent} from "./login/login.component";

export const routes: Routes = [
  {
    path: 'user',
    children: [
      {
        path: 'login/:error',
        component: LoginComponent
      },
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  }
];
