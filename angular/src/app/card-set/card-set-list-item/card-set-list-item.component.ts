import {Component, Input, OnInit} from '@angular/core';
import {CardSet} from "../card-set";

@Component({
  selector: 'app-card-set-list-item',
  templateUrl: './card-set-list-item.component.html',
  styleUrls: ['./card-set-list-item.component.scss']
})
export class CardSetListItemComponent implements OnInit {

  @Input() cardSet: CardSet;

  constructor() { }

  ngOnInit() {
  }

}
