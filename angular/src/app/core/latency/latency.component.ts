import {Component, OnInit} from '@angular/core';
import {PingService} from "../../socket/ping.service";

const HIGH_LATENCY = 500;

@Component({
  selector: 'app-latency',
  templateUrl: './latency.component.html',
  styleUrls: ['./latency.component.scss']
})
export class LatencyComponent implements OnInit {

  public isHigh: boolean = false;
  public latency: number = 0;

  constructor(private _pingService: PingService) {
    this._pingService.latency$
      .subscribe(latency => this.updateLatency(latency));
  }

  ngOnInit() {
  }

  protected updateLatency(latency: number) {
    this.latency = latency;
    this.isHigh = this.latency >= HIGH_LATENCY;
  }

}
