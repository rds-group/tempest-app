import {ModelChanges} from "./model-changes";

export interface ModelUpdate {

  modelUpdate(data: any, changes?: ModelChanges);

}
