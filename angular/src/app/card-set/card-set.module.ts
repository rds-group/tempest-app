import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {CardModule} from "../card/card.module";
import {CardSetService} from "./card-set.service";
import {CardSetListComponent} from './card-set-list/card-set-list.component';
import {CardSetListItemComponent} from './card-set-list-item/card-set-list-item.component';
import {CardSetDetailComponent} from './card-set-detail/card-set-detail.component';
import {routes} from "./routes";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    SharedModule,
    CardModule
  ],
  declarations: [
    CardSetListComponent,
    CardSetListItemComponent,
    CardSetDetailComponent
  ],
  providers: [
    CardSetService
  ]
})
export class CardSetModule {
}
