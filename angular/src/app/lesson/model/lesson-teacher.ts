import {LessonUser} from "./lesson-user";
import {AppAction} from "../../shared/app-action";
import {LessonTeacherData} from "../data/lesson-teacher-data";

export class LessonTeacher extends LessonUser implements LessonTeacherData {

  constructor(data?: any) {
    super(data);

    this._pinAddAction = AppAction.lessonTeacherPinAdd;
    this._pinRemoveAction = AppAction.lessonTeacherPinRemove;
  }

}
