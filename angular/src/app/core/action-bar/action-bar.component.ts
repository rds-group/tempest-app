import {Component, EventEmitter, InjectionToken, Injector, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {UserService} from "../../user/user.service";
import {User} from "../../user/user";
import {LessonService} from "../../lesson/lesson.service";
import {Lesson} from "../../lesson/model/lesson";
import * as _ from "lodash";
import {OnNavigationBack} from "../../shared/on-navigation-back";

@Component({
  selector: 'app-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

  @Input() studentListEnabled: boolean;
  @Output() studentListEnabledChange: EventEmitter<boolean> = new EventEmitter();

  @Input() contextBarEnabled: boolean;
  @Output() contextBarEnabledChange: EventEmitter<boolean> = new EventEmitter();

  public isRegistered: boolean;
  public isPreviousCard: boolean;
  public isNextCard: boolean;
  public user: User;
  public userType = User.Type;
  public lesson: Lesson;
  public onBack: any;

  constructor(
    private _injector: Injector,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private _lessonService: LessonService
  ) {}

  ngOnInit() {
    this._userService.user$.subscribe(user => this.user = user);

    this._userService.isRegistered$.subscribe(isRegistered => {
      this.isRegistered = isRegistered;

      this.contextBarEnabled = false;
      this.contextBarEnabledChange.emit(this.contextBarEnabled);

      this.studentListEnabled = false;
      this.studentListEnabledChange.emit(this.studentListEnabled);
    });

    this._lessonService.lesson$.subscribe(lesson => {
      this.lesson = lesson;

      if (lesson) {
        this.isPreviousCard = this._lessonService.isPreviousCard();
        this.isNextCard = this._lessonService.isNextCard();
      }
    });

    this.bindNavigationBack();
  }

  public onLockLessonClick() {
    if (this.user.type == User.Type.Teacher)
      this._lessonService.lock(!this.lesson.isLocked);
  }

  public onClearLessonClick() {
    this._lessonService.clear();
  }

  public onPreviousCardClick() {
    this._lessonService.previousCard().subscribe({error: () => {}});
  }

  public onNextCardClick() {
    this._lessonService.nextCard().subscribe({error: () => {}});
  }

  public onStudentListClick() {
    this.studentListEnabled = !this.studentListEnabled;
    this.studentListEnabledChange.emit(this.studentListEnabled);
  }

  public onLogoutClick() {
    this._userService.logout();
    this._lessonService.dispose();
  }

  private bindNavigationBack() {
    this._router.events.filter(e => e instanceof NavigationEnd)
    .switchMap(() => {
      let route: ActivatedRoute = this._activatedRoute.firstChild;

      while (route.firstChild)
        route = route.firstChild;

      return route.data;
    })
    .subscribe(data => {
      const navigationBack: any = data.navigationBack;

      if (_.isArray(navigationBack))
        this.onBack = () => this._router.navigate(navigationBack);

      else if (navigationBack instanceof InjectionToken) {
        const service: OnNavigationBack = this._injector.get(navigationBack);
        this.onBack = service.isEnabled() ? () => service.onNavigationBack() : null;
      }

      else
        this.onBack = null;
    });
  }

}
