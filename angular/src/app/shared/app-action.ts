export class AppAction {

  public static userRegister: string = 'user.register';
  public static userUnregister: string = 'user.unregister';
  public static userAdd: string = 'user.add';
  public static userRemove: string = 'user.remove';
  public static userStatus: string = 'user.status';

  public static registeredUsersUpdate: string = 'registered-users.update';

  public static lessonStart: string = 'lesson.start';
  public static lessonEnd: string = 'lesson.end';
  public static lessonUpdate: string = 'lesson.update';
  public static lessonClear: string = 'lesson.clear';
  public static lessonLock: string = 'lesson.lock';
  public static lessonUnlock: string = 'lesson.unlock';
  public static lessonCard: string = 'lesson.card';
  public static lessonTeacherClear: string = 'lesson.teacher.clear';
  public static lessonTeacherPinAdd: string = 'lesson.teacher.pin.add';
  public static lessonTeacherPinRemove: string = 'lesson.teacher.pin.remove';
  public static lessonStudentAdd: string = 'lesson.student.add';
  public static lessonStudentRemove: string = 'lesson.student.remove';
  public static lessonStudentClear: string = 'lesson.student.clear';
  public static lessonStudentPinAdd: string = 'lesson.student.pin.add';
  public static lessonStudentPinRemove: string = 'lesson.student.pin.remove';

}
