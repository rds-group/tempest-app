import {LessonUser} from "./lesson-user";
import {ModelChanges} from "../../shared/model-update/model-changes";
import {LessonPin} from "./lesson-pin";
import {ModelChange} from "../../shared/model-update/model-change";
import {AppAction} from "../../shared/app-action";
import {PinData} from "../../shared/data/pin-data";
import {LessonStudentData} from "../data/lesson-student-data";

export class LessonStudent extends LessonUser implements LessonStudentData {

  constructor(data?: any) {
    super(data);

    this._pinAddAction = AppAction.lessonStudentPinAdd;
    this._pinRemoveAction = AppAction.lessonStudentPinRemove;
  }

}
