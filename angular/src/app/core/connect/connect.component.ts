import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from "../../user/user.service";
import {CardService} from "../../card/card.service";
import {User} from "../../user/user";
import {Router} from "@angular/router";
import {LessonService} from "../../lesson/lesson.service";
import {Subscription} from "rxjs";
import {SocketService} from "../../socket/socket.service";
import {Observable} from "rxjs/Observable";
import {CardSetService} from "../../card-set/card-set.service";

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent implements OnInit, OnDestroy {

  public isStudentConnected: boolean = false;
  private _connectSub: Subscription;
  private _lessonSub: Subscription;

  constructor(
    private _router: Router,
    private _socketService: SocketService,
    private _userService: UserService,
    private _cardSetService: CardSetService,
    private _lessonService: LessonService
  ) {}

  ngOnInit() {
    if (this._userService.isRegistered$.getValue())
      this.onConnect();

    else {
      this._connectSub = this._socketService.connection$.asObservable()
      .subscribe(connection => {
        if (!connection.isOpen)
          return;

        this._userService.register()
        .catch(error => {
          this._userService.logout();
          this._router.navigate(['/user/login', error]);

          return Observable.throw(null);
        })
        .concat(this._cardSetService.loadCardSets())
        .subscribe({complete: () => this.onConnect()});
      });
    }
  }

  ngOnDestroy() {
    if (this._connectSub)
      this._connectSub.unsubscribe();

    if (this._lessonSub)
      this._lessonSub.unsubscribe();
  }

  private onConnect() {
    const user: User = this._userService.user$.getValue();

    if (user.type == User.Type.Teacher)
      this._router.navigate(['/card-set']);

    else {
      this.isStudentConnected = true;

      this._lessonSub = this._lessonService.lesson$.asObservable()
      .subscribe(lesson => {
        if (!lesson)
          return;

        this._router.navigate(['/lesson']);
      });
    }
  }

}
