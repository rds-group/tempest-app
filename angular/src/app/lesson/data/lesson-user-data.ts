import {PinData} from "../../shared/data/pin-data";

export interface LessonUserData {

  userId: string;
  pins: PinData[];

}
