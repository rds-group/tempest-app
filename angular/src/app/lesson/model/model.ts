import {ModelUpdate} from "../../shared/model-update/model-update";
import {ModelChanges} from "../../shared/model-update/model-changes";

declare const _: any;

export class Model implements ModelUpdate {

  constructor(data?: any) {
    if (data)
      _.assign(this, data);
  }

  modelUpdate(data: any, changes: ModelChanges) {}

}
