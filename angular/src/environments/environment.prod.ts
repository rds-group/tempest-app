export const environment = {
  production: true
};

export const config: any = {

  server: {
    url: 'http://91.219.209.131:3002/'
  },

  socket: {
    url: 'ws://91.219.209.131:3002/'
  }

};
