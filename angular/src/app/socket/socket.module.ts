import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";

import {SocketService} from "./socket.service";

import {ConnectComponent} from '../core/connect/connect.component';

import {routes} from "./routes";
import {PingService} from "./ping.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ConnectComponent],
  providers: [
    SocketService,
    PingService
  ]
})
export class SocketModule {
}
