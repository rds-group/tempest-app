import {Observable} from "rxjs/Observable";

export class SocketConnection {

  public isOpen: boolean = false;
  public socket: any;

  public on(eventName: string): Observable<any> {
    return Observable.create(observer => {
      const handler: any = res => observer.next(res);

      this.socket.on(eventName, handler);

      return () => this.socket.off(eventName, handler);
    })
    .do(data => console.log('socket on', eventName, data));
  }

}
