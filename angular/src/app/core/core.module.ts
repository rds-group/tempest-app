import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OverseerService} from "./overseer.service";

import {VersionComponent} from './version/version.component';
import {NotificationComponent} from './notification/notification.component';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {StudentListComponent} from './student-list/student-list.component';
import {ContextBarComponent} from './context-bar/context-bar.component';
import {UserPipe} from './user.pipe';
import {LatencyComponent} from './latency/latency.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    VersionComponent,
    NotificationComponent,
    ActionBarComponent,
    StudentListComponent,
    ContextBarComponent,
    UserPipe,
    LatencyComponent
  ],
  providers: [
    OverseerService
  ],
  exports: [
    VersionComponent,
    NotificationComponent,
    ActionBarComponent,
    StudentListComponent,
    ContextBarComponent,
    LatencyComponent
  ]
})
export class CoreModule {
}
