import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers, Response} from "@angular/http";
import {ConfigService} from "./config.service";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiService {

  public username: string;
  public password: string;
  private _url: string;

  constructor(
    private _config: ConfigService,
    private _http: Http
  ) {
    this._url = this._config.server.url + 'api/';
  }

  public get(path: string): Observable<any> {
    return this._http.get(this._url + path, this.createRequestOptions())
    .map(res => this.onResponse(res))
    .catch(error => this.onError(error));
  }

  public post(path: string, data: any): Observable<any> {
    return this._http.post(this._url + path, data, this.createRequestOptions())
    .map(res => this.onResponse(res))
    .catch(error => this.onError(error));
  }

  private createRequestOptions(): RequestOptions {
    const options: RequestOptions = new RequestOptions();

    if (this.username && this.password) {
      options.headers = new Headers();
      options.headers.append('Authorization', 'Basic ' + btoa(this.username + ":" + this.password));
    }

    return options;
  }

  private onResponse(res: Response) {
    if (res.ok)
      return res.json();

    return this.onError(res);
  }

  private onError(error: any) {
    let message: string = error instanceof Response ? error.json().message : error;
    if (!message)
      message = 'An error occurred.';

    return Observable.throw(message);
  }

}
