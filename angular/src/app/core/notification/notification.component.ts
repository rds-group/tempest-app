import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../shared/api.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  public notifications: Object[] = [];

  constructor(private _apiService: ApiService) {}

  ngOnInit() {}

}
