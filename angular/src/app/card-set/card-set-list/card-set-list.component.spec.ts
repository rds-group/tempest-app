import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSetListComponent } from './card-set-list.component';

describe('CardSetListComponent', () => {
  let component: CardSetListComponent;
  let fixture: ComponentFixture<CardSetListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSetListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
