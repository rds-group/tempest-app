import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";
import {SharedModule} from "./shared/shared.module";
import {CoreModule} from "./core/core.module";
import {SocketModule} from "./socket/socket.module";
import {UserModule} from "./user/user.module";
import {CardModule} from "./card/card.module";
import {CardSetModule} from "./card-set/card-set.module";
import {LessonModule} from "./lesson/lesson.module";
import {AppComponent} from './app.component';
import {routes} from './routes';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    SharedModule,
    CoreModule,
    SocketModule,
    UserModule,
    CardModule,
    CardSetModule,
    LessonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
