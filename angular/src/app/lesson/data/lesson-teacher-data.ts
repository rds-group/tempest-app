import {LessonUserData} from "./lesson-user-data";

export interface LessonTeacherData extends LessonUserData {}
