import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from "@angular/router";
import {CardModule} from "../card/card.module";
import {LessonService} from "./lesson.service";
import {NavigationBackService} from "./navigation-back.service";
import {LessonComponent} from './lesson/lesson.component';
import {navigationBack, routes} from "./routes";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    CardModule
  ],
  declarations: [LessonComponent],
  providers: [
    LessonService,
    {provide: navigationBack, useClass: NavigationBackService}
  ]
})
export class LessonModule {
}
