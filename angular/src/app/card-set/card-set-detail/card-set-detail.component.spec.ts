import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSetDetailComponent } from './card-set-detail.component';

describe('CardSetDetailComponent', () => {
  let component: CardSetDetailComponent;
  let fixture: ComponentFixture<CardSetDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSetDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
