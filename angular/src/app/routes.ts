import {Routes} from "@angular/router";
import {ConnectComponent} from "./core/connect/connect.component";
import {UserGuard} from "./user/user-guard.service";

export const routes: Routes = [
  {
    path: 'connect',
    component: ConnectComponent,
    canActivate: [UserGuard]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/user/login'
  }
];
