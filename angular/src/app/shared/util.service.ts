import {Injectable} from '@angular/core';
import {ConfigService} from "./config.service";
import * as _ from "lodash";

@Injectable()
export class UtilService {

  constructor(private _configService: ConfigService) { }

  public fixServerUrl(url: string): string {
    return this._configService.server.url + _.trimStart(url, '/');
  }

}
