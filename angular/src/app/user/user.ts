import {UserData} from "./user-data";
import * as _ from "lodash";
import {ModelUpdate} from "../shared/model-update/model-update";
import {ModelChanges} from "../shared/model-update/model-changes";

export class User implements UserData, ModelUpdate {

  public _id: string;
  public type: User.Type;
  public username: string;
  public password: string;
  public displayName: string;
  public symbol: string;
  public color: string;
  public isActive: boolean;

  constructor(data?: any) {
    if (data)
      this.setData(data);
  }

  modelUpdate(data: UserData, changes?: ModelChanges) {
    this.setData(data);
  }

  private setData(data: UserData) {
    _.assign(this, data);

    if (_.isString(data.type))
      this.type = data.type == 'teacher' ? User.Type.Teacher : User.Type.Student;
  }

}

export namespace User {

  export enum Type {

    Teacher,
    Student

  }

}
