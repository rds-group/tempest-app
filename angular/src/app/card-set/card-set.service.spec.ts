import { TestBed, inject } from '@angular/core/testing';

import { CardSetService } from './card-set.service';

describe('CardSetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardSetService]
    });
  });

  it('should ...', inject([CardSetService], (service: CardSetService) => {
    expect(service).toBeTruthy();
  }));
});
