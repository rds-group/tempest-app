export class Card {

  public _id: string;
  public cardSet: string;
  public name: string;
  public order: number;
  public preview: string;
  public version: number;
  public data: Object;
  public pinLimit: number;

}
