import {
  AfterViewInit, Component, ElementRef, EventEmitter, Input, NgZone, OnChanges, OnDestroy, SimpleChanges,
  ViewChild
} from '@angular/core';
import {PinData} from "../../shared/data/pin-data";
import {CardPinType} from "../card-pin-type.enum";
import * as _ from "lodash";

declare const fabric: any;

@Component({
  selector: 'app-fabric',
  templateUrl: './fabric.component.html',
  styleUrls: ['./fabric.component.scss']
})
export class FabricComponent implements AfterViewInit, OnChanges, OnDestroy {

  @Input() data: Object;

  @ViewChild('canvasWrapper') private _canvasWrapperRef: ElementRef;

  private CANVAS_WIDTH: number = 1280;
  private CANVAS_HEIGHT: number = 720;

  public onClick: EventEmitter<{ x: number, y: number }> = new EventEmitter();
  public onObjectClick: EventEmitter<any> = new EventEmitter();
  private _canvas: any;
  private _objects: any[] = [];
  private _scalingIntervalId: any;
  private _scalingDimensions: { w: number, h: number } = null;

  constructor(
    private _element: ElementRef,
    private _ngZone: NgZone
  ) {}

  ngAfterViewInit() {
    this.initCanvas();
    this.updateData();
    this.setupScaling();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && changes.data.currentValue)
      this.updateData();
  }

  ngOnDestroy() {
    window.clearInterval(this._scalingIntervalId);
  }

  public addPin(data: { data: PinData, type: CardPinType, color: string, label: string }): any {
    let shape: any;

    if (data.type == CardPinType.Default)
      shape = new fabric.Circle({radius: 30, fill: data.color, originX: 'center', originY: 'center'});
    else
      shape = new fabric.Rect({
        width: 60,
        height: 60,
        angle: 45,
        fill: data.color,
        originX: 'center',
        originY: 'center'
      });

    const label: any = new fabric.Text(data.label, {
      fontFamily: 'Arial',
      fontWeight: 'bold',
      fontSize: 24,
      fill: '#ffffff',
      originX: 'center',
      originY: 'center'
    });
    const pin: any = new fabric.Group([shape, label], {
      left: data.data.x,
      top: data.data.y,
      originX: 'center',
      originY: 'center'
    });
    pin.setShadow({offsetX: 0, offsetY: 10, blur: 30, color: 'rgba(0, 0, 0, 0.35)'});
    this._objects.push(pin);

    if (this._canvas)
      this._canvas.add(pin);

    return pin;
  }

  public removePin(object: any) {
    if (this._canvas)
      this._canvas.remove(object);

    _.remove(this._objects, object);
  }

  private initCanvas() {
    this._canvas = new fabric.Canvas('canvas', {
      width: this.CANVAS_WIDTH,
      height: this.CANVAS_HEIGHT,
      interactive: false,
      selection: false
    });

    this._canvas.on('mouse:down', options => this.onCanvasClick(options));
  }

  private updateData() {
    if (this._canvas && this.data) {
      this._canvas.loadFromJSON(this.data, () => {
        const objects: any[] = this._canvas.getObjects();
        _.forEach(objects, object => {
          object.selectable = false;
        });

        this._canvas.renderAll.bind(this._canvas)();
        this._canvas.add(...this._objects);
      });
    }
  }

  private setupScaling() {
    this._ngZone.runOutsideAngular(() => {
      this._scalingIntervalId = window.setInterval(() => {
        const maxW: number = this._element.nativeElement.clientWidth;
        const maxH: number = this._element.nativeElement.clientHeight;

        if (this._scalingDimensions && this._scalingDimensions.w == maxW && this._scalingDimensions.h == maxH)
          return;

        const maxR: number = maxW / maxH;
        const cntR: number = this.CANVAS_WIDTH / this.CANVAS_HEIGHT;
        let w: number;
        let h: number;

        if (maxR > cntR) {
          h = maxH;
          w = maxH * cntR;
        }
        else {
          w = maxW;
          h = maxW / cntR;
        }

        this._canvasWrapperRef.nativeElement.style.width = w + 'px';
        this._canvasWrapperRef.nativeElement.style.height = h + 'px';

        this._scalingDimensions = {w: maxW, h: maxH};
      }, 100);
    });
  }

  private onCanvasClick(options: { e: Event, target: any }) {
    let x: number;
    let y: number;

    if (options.e instanceof MouseEvent) {
      const e: MouseEvent = options.e as MouseEvent;
      x = e.clientX;
      y = e.clientY;
    } else {
      const touch: Touch = (options.e as TouchEvent).touches[0];
      x = touch.clientX;
      y = touch.clientY;
    }

    const rect: any = this._canvasWrapperRef.nativeElement.getBoundingClientRect();
    x = Math.round((x - rect.left) / rect.width * this.CANVAS_WIDTH);
    y = Math.round((y - rect.top) / rect.height * this.CANVAS_HEIGHT);

    const point: any = new fabric.Point(x, y);
    const object: any = _.findLast(this._objects, o => o.containsPoint(point));

    if (object)
      this.onObjectClick.emit(object);

    else
      this.onClick.emit({x: x, y: y});
  }

}
