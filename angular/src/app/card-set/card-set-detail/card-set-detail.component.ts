import {Component, OnInit} from '@angular/core';
import {CardSetService} from "../card-set.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CardSet} from "../card-set";
import {Card} from "../../card/card";
import {LessonService} from "../../lesson/lesson.service";

@Component({
  selector: 'app-card-set-detail',
  templateUrl: './card-set-detail.component.html',
  styleUrls: ['./card-set-detail.component.scss']
})
export class CardSetDetailComponent implements OnInit {

  public cardSet: CardSet;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _cardSetService: CardSetService,
    private _lessonService: LessonService
  ) { }

  ngOnInit() {
    this._activatedRoute.params.switchMap(params => this._cardSetService.getCardSet(params['id']))
    .subscribe(cardSet => this.cardSet = cardSet);
  }

  public onCardClick(card: Card) {
    this._lessonService.start(this.cardSet, card._id)
    .subscribe(() => this._router.navigate(['lesson']));
  }

}
