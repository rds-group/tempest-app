export interface PinData {

  id: string;
  x: number;
  y: number;

}
