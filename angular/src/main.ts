import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

if (window.navigator.platform == 'Win32')
  bootstrap();
else
  document.addEventListener('deviceready', bootstrap);

function bootstrap() {
  platformBrowserDynamic().bootstrapModule(AppModule);
}
