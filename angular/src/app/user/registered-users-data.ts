import {UserData} from "./user-data";

export interface RegisteredUsersData {

  users: UserData[];

}
