import {Component, OnInit} from '@angular/core';
import {CardSetService} from "../card-set.service";
import {CardSet} from "../card-set";
import {Router} from "@angular/router";
import * as _ from "lodash";

@Component({
  selector: 'app-card-set-list',
  templateUrl: './card-set-list.component.html',
  styleUrls: ['./card-set-list.component.scss']
})
export class CardSetListComponent implements OnInit {

  public cardSets: CardSet[];
  public tagFilter: string;

  constructor(
    private _router: Router,
    private _cardSetService: CardSetService
  ) { }

  ngOnInit() {
    this.cardSets = this._cardSetService.cardSets$.value;
  }

  public filter() {
    this.cardSets = _.filter(this._cardSetService.cardSets$.value, cardSet => {
      if (this.tagFilter.length == 0) return true;
      return _.some(cardSet.tags, tag => _.includes(tag, this.tagFilter));
    });
  }

  public onCardSetClick(cardSet: CardSet) {
    this._router.navigate(['card-set', cardSet._id]);
  }

}
