import {PinData} from "../shared/data/pin-data";

declare const _: any;

export class CardPin {

  public data: PinData;
  public color: string;
  public fabricObject: any;

  constructor(data?: any) {
    if (data)
      _.assign(this, data);
  }

}
