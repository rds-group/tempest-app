import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {LessonService} from "../lesson.service";
import {Card} from "../../card/card";
import {Subscription} from "rxjs/Subscription";
import {CardService} from "../../card/card.service";
import {AppAction} from "../../shared/app-action";
import {CardComponent} from "../../card/card/card.component";
import {LessonPin} from "../model/lesson-pin";
import {LessonUser} from "../model/lesson-user";
import {User} from "../../user/user";
import {UserService} from "../../user/user.service";
import {Router} from "@angular/router";
import {PinData} from "../../shared/data/pin-data";
import {Lesson} from "../model/lesson";
import {CardPinType} from "../../card/card-pin-type.enum";

declare const _: any;

@Component({
  selector: 'app-lesson',
  templateUrl: './lesson.component.html',
  styleUrls: ['./lesson.component.scss']
})
export class LessonComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild(CardComponent) private _cardCom: CardComponent;

  public card: Card;
  private _lessonSub: Subscription;
  private _changesSub: Subscription;

  constructor(
    private _router: Router,
    private _lessonService: LessonService,
    private _cardService: CardService,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this.bindLesson();
    this.bindChanges();
  }

  ngAfterViewInit() {
    this._cardCom.onPinAdd.subscribe(data => this.onCardPinAdd(data));
    this._cardCom.onPinRemove.subscribe(pin => this.onCardPinRemove(pin));
  }

  ngOnDestroy() {
    this._lessonSub.unsubscribe();
    this._changesSub.unsubscribe();
  }

  private bindLesson() {
    this._lessonSub = this._lessonService.lesson$.asObservable()
    .subscribe(lesson => {
      if (!lesson) {
        const user: User = this._userService.user$.value;

        if (user && user.type == User.Type.Teacher && this.card)
          this._router.navigate(['card-set', this.card.cardSet]);

        else
          this._router.navigate(['connect']);
      }
    });
  }

  private bindChanges() {
    this._changesSub = this._lessonService.changes$.asObservable()
    .subscribe(changes => {
      if (!changes)
        return;

      _.forEach(changes, change => {
        if (change.action == AppAction.lessonCard) {
          this._cardService.getCard(change.data.cardId)
          .subscribe(card => this.card = card);
        }
        else if (_.includes([AppAction.lessonTeacherPinAdd, AppAction.lessonStudentPinAdd], change.action))
          this.addPin(change.data);

        else if (_.includes([AppAction.lessonTeacherPinRemove, AppAction.lessonStudentPinRemove], change.action))
          this.removePin(change.data);
      });
    });
  }

  private addPin(pin: LessonPin) {
    const lessonUser: LessonUser = this._lessonService.getUserByPin(pin);
    if (!lessonUser) {
      console.error('LessonComponent@addPin lesson user not found');
      return;
    }

    const pinUser: User = this._userService.getRegisteredUser(lessonUser.userId);
    if (!pinUser) {
      console.error('LessonComponent@addPin user not found');
      return;
    }

    const user: User = this._userService.getRegisteredUser();

    if (user == pinUser || user.type == User.Type.Teacher || pinUser.type == User.Type.Teacher) {
      const data: any = {
        data: pin,
        type: pinUser.type == User.Type.Teacher ? CardPinType.Special : CardPinType.Default,
        color: pinUser.color,
        label: pinUser.symbol
      };

      this._cardCom.addPin(data);
    }
  }

  private removePin(pin: LessonPin) {
    this._cardCom.removePin(pin);
  }

  private onCardPinAdd(data: {x: number, y: number}) {
    const lesson: Lesson = this._lessonService.lesson$.getValue();
    if (!lesson) {
      console.error('LessonComponent@onCardPinAdd lesson not found');
      return;
    }

    const user: User = this._userService.user$.getValue();

    if (user.type == User.Type.Student && lesson.isLocked)
      return;

    const lessonUser: LessonUser = this._lessonService.getUser();
    if (!lessonUser) {
      console.error('LessonComponent@onCardPinAdd lesson user not found');
      return;
    }

    if (user.type == User.Type.Student && lessonUser.pins.length >= this.card.pinLimit)
      return;

    this._lessonService.addPin(data);
  }

  private onCardPinRemove(pin: PinData) {
    const lesson: Lesson = this._lessonService.lesson$.getValue();
    if (!lesson) {
      console.error('LessonComponent@onCardPinRemove lesson not found');
      return;
    }

    const user: User = this._userService.user$.getValue();

    if (user.type == User.Type.Student && lesson.isLocked)
      return;

    const lessonUser: LessonUser = this._lessonService.getUserByPin(pin);
    if (!lessonUser) {
      console.error('LessonComponent@removePin lesson user not found');
      return;
    }

    const pinUser: User = this._userService.getRegisteredUser(lessonUser.userId);
    if (!pinUser) {
      console.error('LessonComponent@removePin user not found');
      return;
    }

    const registeredUser: User = this._userService.getRegisteredUser();

    if (registeredUser == pinUser)
      this._lessonService.removePin(pin);
  }

}
