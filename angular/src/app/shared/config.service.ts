import {Injectable} from '@angular/core';
import {config} from "../../environments/environment";
import * as _ from "lodash";

interface server {url: string}
interface socket {url: string}
interface user {username: string, password: string}

@Injectable()
export class ConfigService {

  public server: server = {
    url: 'http://10.10.112.13:3002/'
  };

  public socket: socket = {
    url: 'ws://10.10.112.13:3002/'
  };

  public user: user = {
    username: null,
    password: null
  };

  constructor() {
    _.merge(this, config);
  }

}
