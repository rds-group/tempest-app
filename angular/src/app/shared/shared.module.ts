import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TitleComponent} from './title/title.component';
import {LoaderComponent} from './loader/loader.component';
import {ApplicationService} from "./application.service";
import {ConfigService} from "./config.service";
import {ApiService} from "./api.service";
import {UtilService} from "./util.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TitleComponent,
    LoaderComponent
  ],
  providers: [
    ApplicationService,
    ConfigService,
    ApiService,
    UtilService
  ],
  exports: [
    TitleComponent,
    LoaderComponent
  ]
})
export class SharedModule {
}
