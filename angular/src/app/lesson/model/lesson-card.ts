import {ModelChanges} from "../../shared/model-update/model-changes";
import {ModelChange} from "../../shared/model-update/model-change";
import {AppAction} from "../../shared/app-action";
import {Model} from "./model";
import {LessonCardData} from "../data/lesson-card-data";

export class LessonCard extends Model implements LessonCardData {

  public cardId: string;

  modelUpdate(data: LessonCardData, changes: ModelChanges) {
    if (data.cardId != this.cardId) {
      this.cardId = data.cardId;
      changes.push(new ModelChange(AppAction.lessonCard, this));
    }
  }

}
