import {ModelUpdate} from "../shared/model-update/model-update";
import {ModelChanges} from "../shared/model-update/model-changes";
import {RegisteredUsersData} from "./registered-users-data";
import {UserData} from "./user-data";
import {User} from "./user";
import {ModelChange} from "../shared/model-update/model-change";
import {AppAction} from "../shared/app-action";

declare const _: any;

export class RegisteredUsers implements RegisteredUsersData, ModelUpdate {

  public users: UserData[] = [];

  modelUpdate(data: RegisteredUsersData, changes?: ModelChanges) {
    this.updateUsers(data.users, changes);
  }

  public clear(changes?: ModelChanges) {
    while (this.users.length)
      this.removeUser(this.users[0], changes);
  }

  public addUser(data: UserData, changes?: ModelChanges) {
    const user: User = new User(data);
    this.users.push(user);

    if (changes)
      changes.push(new ModelChange(AppAction.userAdd, user));
  }

  public removeUser(data: UserData, changes?: ModelChanges) {
    const user: User = _.find(this.users, u => u._id == data._id);

    if (!user) {
      console.log('RegisteredUsers@removeUser user not found, id:', data._id);
      return;
    }

    this.users.splice(_.indexOf(this.users, user), 1);

    if (changes)
      changes.push(new ModelChange(AppAction.userRemove, user));
  }

  public getUser(id: string): User {
    return _.find(this.users, u => u._id == id);
  }

  private updateUsers(data: UserData[], changes?: ModelChanges) {
    const users: UserData[] = _.unionBy(this.users, data, u => u._id);

    _.forEach(users, user => {
      const indexInCurrent: number = _.findIndex(this.users, u => u._id == user._id);
      const indexInNew: number = _.findIndex(data, u => u._id == user._id);

      if (indexInCurrent != -1 && indexInNew != -1)
        (<User>user).modelUpdate(data[indexInNew]);

      else if (indexInCurrent != -1)
        this.removeUser(user, changes);

      else
        this.addUser(user, changes);
    });
  }

}
