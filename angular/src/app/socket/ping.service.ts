import {Injectable} from '@angular/core';
import {SocketService} from "./socket.service";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

const PING_TIME = 2000; //2 seconds
const REPORT_TIME = 60000; //1 minute

@Injectable()
export class PingService {
  private _pingInterval;
  private _reportInterval;

  public latency$: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor(private _socketService: SocketService) {
    this.bindSocket();
  }

  protected ping() {
    this._socketService.emit('latency.ping', Date.now())
      .subscribe(pingTime => {
        this.latency$.next(Date.now() - pingTime);
      });
  }

  protected report() {
    this._socketService.emit('latency.report', this.latency$.getValue())
      .subscribe();
  }

  protected bindSocket() {
    this._socketService.connection$.subscribe(connection => {
      if (connection.isOpen && !this._pingInterval && !this._reportInterval) {
        this._pingInterval = setInterval(() => this.ping(), PING_TIME);
        this._reportInterval = setInterval(() => this.report(), REPORT_TIME);
      } else {
        clearInterval(this._pingInterval);
        clearInterval(this._reportInterval);
        this._pingInterval = null;
        this._reportInterval = null;
      }
    });
  }

}
