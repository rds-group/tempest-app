import {Pipe, PipeTransform} from '@angular/core';
import {User} from "../user/user";

declare const _: any;

@Pipe({
  name: 'user',
  pure: false
})
export class UserPipe implements PipeTransform {

  transform(users: User[], userType: User.Type): any {
    if (!users || !userType) {
      return users;
    }

    return _.filter(users, user => user.type == userType);
  }

}
