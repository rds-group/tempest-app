import { TestBed, inject } from '@angular/core/testing';

import { NavigationBackService } from './navigation-back.service';

describe('NavigationBackService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NavigationBackService]
    });
  });

  it('should ...', inject([NavigationBackService], (service: NavigationBackService) => {
    expect(service).toBeTruthy();
  }));
});
